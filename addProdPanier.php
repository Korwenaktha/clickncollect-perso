<?php
if( isset($_POST['id_prod']) ) {
    $id_commande = $_COOKIE['id_commande'];
    $id_produit = $_POST['id_prod'];
    $qt = $_POST['qt'];

    include_once('pdo.php');

    if($id_commande == "") {
        $req = $pdo->prepare('insert into commande() values ();');
        $req->execute();
        $id_commande = $pdo->lastInsertId();
        setcookie("id_commande", $id_commande);
    }

    $req = $pdo->prepare('INSERT INTO produit_commande (id_commande,id_produit,qt) 
        VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE qt=qt+?;');
    $req->execute([$id_commande, $id_produit, $qt, $qt]);
}

header("location:boutique.php");
?>


<?php

session_start(); // Démarre une nouvelle session ou restaure une session existante

// Si le panier n'existe pas encore en session, on le crée sous forme de tableau vide
if (!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = array();
}

// Fonction pour ajouter un produit au panier
function addProdCart($produit_id) {
    $_SESSION['panier'][$produit_id];
}

// Fonction pour supprimer un produit du panier
function deleteProdCart($produit_id) {
    unset($_SESSION['panier'][$produit_id]);
}

// Fonction pour vider le panier
function viderPanier() {
    $_SESSION['panier'] = array();
}

// Fonction pour récupérer le contenu du panier
function getCartContain() {
    return $_SESSION['panier'];
}

?>