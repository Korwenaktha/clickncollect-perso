<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="cnc.css">
</head>

<body>

    <header class="btqHeader">
        <img class="logo" src="https://picsum.photos/200?random=3" alt="">
        <div class="btqPresent">
            Le Cymbalier
        </div>
        <div class="btqProd">
            Boutique
        </div>
        <input type=button class="panier" onClick="parent.location='panier.php'" 
            value="Panier (3<?php $nbArticle ?>)">
        
    </header>

    <div id="liste">
        <?php
            require_once ("pdo.php");

            $req = $pdo->query("select * from produit;");
            $mesInfos = $req->fetchAll();

            foreach($mesInfos as $data){
        ?>

            <div class="produit">
                <img src="<?= $data["image"] ?>" alt="<?= $data["nom"] ?>">
                <div>
                    <h2><?= $data["nom"] ?></h2>
                    <p><span><?= $data["description"] ?></span></p>
                    <p><span><?= $data["prix"] ?></span>€</p>
                </div>
                <div>
                    <form action="addProdPanier.php" method="post">
                        <input type="hidden" name="id_prod" value="<?= $data["id"] ?>">
                        <input type="hidden" name="qt" value="1">
                        <input type="submit" value="Ajouter au panier">
                    </form>
                </div>
            </div>

        <?php } ?>

    </div>

</body>

</html>

