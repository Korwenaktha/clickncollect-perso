drop database if exists cymbales;
create database cymbales;
use cymbales;

create table produit(
    id int auto_increment primary key,
    image varchar(255),
    nom varchar(255),
    description varchar(3000),
    prix float,
    dispo boolean
);


create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum("panier", "validée", "prete", "collectée") default "panier"
);

create table produit_commande(
    id_commande int,
    id_produit int,
    qt float,
    primary key (id_commande, id_produit)
);

create table client (
    id int auto_increment primary key,
    mail varchar(255),
    nom varchar(255),
    tel varchar(255)
);

drop user if exists korwen@"127.0.0.1";
create user korwen@"127.0.0.1" identified by "Phenix85";
grant all privileges on cymbales.* to korwen@"127.0.0.1";
