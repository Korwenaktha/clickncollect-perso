<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="cnc.css">

</head>


<body>
<?php
    include_once ("pdo.php");
        

    $req = $pdo->query("select * from commande;");
    $mesInfos = $req->fetchAll();

    foreach($mesInfos as $cmd){
        if($cmd["id_client"] != NULL){
            $req = $pdo->query("select * from client where id=${cmd[`id_client`]};");
            $client = $req->fetch();
                
        }else{
            $client = [
                "id" => "",
                "nom" => "",
                "tel" => "",
                "mail" => ""
            ];
        }?>
        <div>
            <div class="boxCmd" style="border: 1px solid black; margin: 3em;"> 

                <div>
                    <span hidden><?= $cmd["id"] ?></span>
                    <span><?= $client["nom"] ?></span>
                    <span><?= $client["mail"] ?></span>
                    <span><?= $client["tel"] ?></span>
                </div>

                <div>
                    <?php

                    $req = $pdo->query("select * from produit;");
                    $mesInfos = $req->fetchAll();

                    $total = 0;

                    foreach($mesInfos as $data){
                        $prod = $pdo->query("select * from produit where id = ${data['id_produit']};")->fetch();
                        $total += $prod["prix"];
                    ?>
                    
                    <li>
                        <?= $prod["nom"] ?> <?= $prod["prix"] ?>€
                    </li>

                    <?php } ?>
                    
                    TOTAL : <?= $total ?>€
                </div>
            </div>
        </div>
    <?php } ?>

</body>

</html>

