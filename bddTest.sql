use cymbales;

insert into produit(image, nom, description, prix, dispo)
values (
    "https://picsum.photos/200?random=1",
    "Cymbale machin",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    225,
    1
);

insert into produit(image, nom, description, prix, dispo)
values (
    "https://picsum.photos/200?random=2",
    "Cymbale truc",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    345,
    1
);

insert into produit(image, nom, description, prix, dispo)
values (
    "https://picsum.photos/200?random=4",
    "Cymbale bidule",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    470,
    1
);

insert into produit(image, nom, description, prix, dispo)
values (
    "https://picsum.photos/200?random=5",
    "Cymbale chose",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    600,
    1
);



-- == CLIENT == 

-- insert into client(mail, nom, tel) values (
--     "bob42@chouette.org",
--     "Bob",
--     "01 02 03 04 87"
-- );

-- insert into client(mail, nom, tel) values (
--     "keanu@chouette.org",
--     "Keanu",
--     "01 02 03 04 87"
-- );